from typing import Optional
from flask import Flask, request, g
from flask_cors import CORS
import sqlite3
import re
from os import environ, listdir
from tqdm import tqdm
from datetime import datetime

app = Flask(__name__)
cors = CORS(app)
app.secret_key = environ.get('SESSIONKEY', 'secret')
if app.secret_key == 'secret':
    print('WARNING: Insecure SESSIONKEY, please set a proper secret key in the environment variable SESSIONKEY')
DATABASE = '/usr/src/app/worlds_data.db'

homepage_world = 'origineel'

# Database connection setup
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

# Initialize database if not exists
def init_db():
    db = get_db()
    cursor = db.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS worlds (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT UNIQUE,
            votes INTEGER,
            world_data TEXT,
            created_at TIMESTAMP
        )
    ''')
    db.commit()
    # Add the default world `original.xml` if it doesn't exist
    cursor.execute("SELECT COUNT(*) FROM worlds WHERE name = ?", (homepage_world,))
    if cursor.fetchone()[0] == 0:
        with open('original.xml') as f:
            cursor.execute("INSERT INTO worlds (name, votes, world_data, created_at) VALUES (?, ?, ?, ?)", (homepage_world, 1, f.read(), None))
        db.commit()


# Database interaction functions
def load_worlds() -> dict:
    db = get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM worlds")
    worlds_data = {row['name']: {'name': row['name'], 'votes': row['votes'], 'world_id': row['id'], 'created_at': row['created_at']} for row in cursor.fetchall()}
    for world in worlds_data.values():
        if world['created_at']:
            world['created_at'] = datetime.fromisoformat(world['created_at'])
    return worlds_data

def read_world(world_name) -> Optional[dict]:
    db = get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM worlds WHERE name = ?", (world_name,))
    result = cursor.fetchone()
    if result:
        result = dict(result)
        if result['created_at']:
            result['created_at'] = datetime.fromisoformat(result['created_at'])
    return result

def worldname_exists(world_name) -> bool:
    db = get_db()
    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM worlds WHERE name = ?", (world_name,))
    return cursor.fetchone()[0] > 0

@app.route('/xml/<world_name>')
def xml_page(world_name):
    world = read_world(world_name)
    if world:
        return world['world_data']
    else:
        return 'World not found', 404

def valid_xml(xml_string: str) -> bool:
    """
    Test if the given XML string is a valid RobbieMaps XML string
    """
    try:
        xml_split = xml_string.split('\n')
        xml_split = [x for x in xml_split if x != '']
        assert xml_split[0] == '<RobbieMaps>'
        assert xml_split[-1] == '</RobbieMaps>'
        for level_string in xml_split[1:-1]:
            assert level_string[:20] == '    <RobbieMap map="'
            assert level_string[-3:] == '"/>'
            actual_data = level_string[20:-3]
            assert len(actual_data) == 216
        return True
    except AssertionError:
        return False
    except IndexError:
        return False

@app.route('/add_world', methods=['POST'])
def add_world():
    world_data = request.values.get('world_data')
    world_name = request.values.get('world_name')

    if not valid_xml(world_data):
        return 'XML test failed: De geleverde level data voldeed niet aan de eisen', 406
    world_name = re.sub(r'[^a-z0-9\s-]', '', world_name.lower()).replace(' ', '-')  # Remove special characters
    if worldname_exists(world_name):
        return 'Wereld naam bestaat al', 406
    if len(world_name) > 128:
        return 'Wereld naam te lang, maximaal 128 tekens', 406

    db = get_db()
    cursor = db.cursor()

    cursor.execute("INSERT INTO worlds (name, votes, world_data, created_at) VALUES (?, ?, ?, ?)", (world_name, 1, world_data, datetime.now()))
    db.commit()

    return world_name, 200

PAGESIZE = 500
@app.route('/get_worlds')
def get_worlds():
    page = int(request.args.get('page', 1))
    sort_by = request.args.get('sort_by')
    order = request.args.get('order')
    if sort_by not in ['score', 'creationDate', 'name']:
        sort_by = 'score'
    worlds = load_worlds()
    worlds_info = [
        {'name': world['name'], 'score': world['votes'], 'creationDate': world['created_at']}
        for world in worlds.values()
    ]
    if sort_by == 'creationDate':
        # We have creationDate of format '2024-11-28 10:38:49.601028' and None's. These can be seen as 1970. Let's sort them safely.
        worlds_info.sort(key=lambda x: x['creationDate'] or datetime.fromisoformat('1970-01-01'))
    elif sort_by == 'name':
        worlds_info.sort(key=lambda x: x['name'])
    else:
        worlds_info.sort(key=lambda x: int(x['score']))
    if order == 'desc':
        worlds_info = worlds_info[::-1]
    
    # Pagination logic
    start = (page - 1) * PAGESIZE
    end = start + PAGESIZE
    paginated_worlds = worlds_info[start:end]
    
    return {'worlds': paginated_worlds, 'total': int(len(worlds_info) / PAGESIZE) + 1}

@app.route('/vote', methods=['POST'])
def vote_up():
    world_name = request.values.get('world_name')
    direction = request.values.get('direction')

    if direction not in ['up', 'down']:
        return 'Invalid direction', 406
    
    db = get_db()
    cursor = db.cursor()

    cursor.execute("SELECT votes FROM worlds WHERE name = ?", (world_name,))
    current_votes = cursor.fetchone()['votes']

    new_votes = current_votes + 1 if direction == 'up' else current_votes - 1

    cursor.execute("UPDATE worlds SET votes = ? WHERE name = ?", (new_votes, world_name))
    db.commit()

    return '', 204

@app.route('/get_votes', methods=['GET'])
def get_votes():
    world_name = request.args.get('world_name')
    db = get_db()
    cursor = db.cursor()

    cursor.execute("SELECT votes FROM worlds WHERE name = ?", (world_name,))
    votes = cursor.fetchone()['votes']

    return {'votes': votes}

@app.route('/get_date', methods=['GET'])
def get_date():
    world_name = request.args.get('world_name')
    db = get_db()
    cursor = db.cursor()

    cursor.execute("SELECT created_at FROM worlds WHERE name = ?", (world_name,))
    created_at = cursor.fetchone()['created_at']

    return {'created_at': created_at}

# Transfer old save files to the database
def transfer_old_files():
    if 'worlds' not in listdir():
        print('No old files to transfer')
        return
    worlds_data = {}
    if 'worlds_data.csv' in listdir():
        with open('worlds_data.csv') as f:
            for line in f.readlines():
                if not line.strip() or line == 'name,votes,world_id\n':
                    continue
                name, votes, world_id = line.strip().split(',')
                worlds_data[world_id] = {'name': name, 'votes': votes, 'world_id': world_id}
    db = get_db()
    cursor = db.cursor()
    world_files = [f for f in listdir('worlds') if f.endswith('.xml')]
    for filename in tqdm(world_files, desc='Transferring old files'):
        with open(f'worlds/{filename}') as f:
            xml = f.read()
        world_id = filename.split('_')[1].split('.')[0]
        world_name = worlds_data.get(world_id, {}).get('name', f"ancient_{world_id}")
        votes = worlds_data.get(world_id, {}).get('votes', 1)
        # If the world already exists, skip it
        cursor.execute("SELECT COUNT(*) FROM worlds WHERE name = ?", (world_name,))
        if cursor.fetchone()[0] == 0:
            cursor.execute("INSERT INTO worlds (name, votes, world_data, created_at) VALUES (?, ?, ?, ?)", (world_name, votes, xml, None))
        else:
            print(f"World {world_name} already exists, skipping")
    db.commit()

@app.route('/ping')
def ping():
    """Connection test"""
    return 'pong'

# Initialize the database before starting the server
# if __name__ == '__main__':
#     with app.app_context():
#         init_db()
#         transfer_old_files()
#     port = int(environ.get("PORT", 5000))
#     app.run(host='0.0.0.0', port=port, debug=True)
