flask==3.1.0
tqdm==4.66.5
flask-cors==5.0.0
gunicorn==23.0.0