#!/bin/bash

# Shortcut for deploying an update from git
# Does:
# - Save the current logs to a backup file if the container exists
# - Stop the current container if it exists
# - Remove the current container if it exists
# - Git pull the new code
# - Docker build the new image
# - Docker run a new container

echo "🚀 Deploying update..."

# Create logs directory if it doesn't exist
mkdir -p logs

# Check if the container is running
if docker ps -a --filter "name=robbie_container" --format '{{.Names}}' | grep -q "robbie_container"; then
  # Save logs before stopping
  echo "📂 Saving logs..."
  docker logs robbie_container --timestamps >& logs/backup-$(date +"%Y-%m-%d_%H-%M-%S").log
  
  # Stop and remove container
  docker stop robbie_container && docker rm robbie_container
  echo "🛑 Stopped and removed existing container..."
fi

# Pull the latest code
git pull

# Build the new image
docker build -t robbie .

# Run the new container
docker run -d --name robbie_container -p 5001:5000 --restart=always -v $(pwd)/worlds_data.db:/usr/src/app/worlds_data.db robbie

echo "✅ Deployment complete!"
