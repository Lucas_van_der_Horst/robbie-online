# Robbie Online
The project is split up into a frontend `public/` and a backend `backend/`.  
The frontend is intended to be served by Gitlab pages.  
The backend is a Python Flask server and is intended to be run in a Docker container.

## Setup
```bash
cd backend
```

~~To run this yourself you'll need some files I can't provide because those are owned by Ambrasoft.~~

~~You'll need to:~~
- ~~Put your `Robbie2D.swf` in `public/flash/`~~
- ~~Put all the tile-textures in `public/tiles` can be ripped from the game file with a tool like [JPEXS Free Flash Decompiler](https://github.com/jindrapetrik/jpexs-decompiler/)~~

Install Python 3.9 or higher.  
And then install the dependencies with:
```bash
pip install -r requirements.txt
```

## Running
For running locally (helpfull for development), this will suffice:
```bash
python app.py
```
**This is not suitable for production. Do not open to the internet!**

## Deployment using Docker
Build the image once (or whenever you update the code):
```bash
docker build -t robbie .
```

And then run the image:
```bash
docker run -d -p 5001:5000 --restart=always -v $(pwd)/worlds_data.db:/usr/src/app/worlds_data.db robbie
```
Make sure there exists a `worlds_data.db` file. If not this can be created by running the app locally once.

## Updating Ruffle

This project currently ships with ruffle nighlty 2022-01-21. When you want to update ruffle, you'll need to:
- Get Russle from [ruffle.rs](https://ruffle.rs/)
- Extract the zip in `public/` and rename the folder to `ruffle`